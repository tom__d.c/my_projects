import re

red = '\033[0;31m'
blue = '\033[0;36m';
green = '\033[0;32m';
white = '\033[0;0m';

regexs = [r'\d\d\d\d\d\d\d\d\d\d\d',r'(\(\d\d\)) (\d) (\d\d\d\d)-(\d\d\d\d)',
r'(\d\d) (\d\d\d\d\d\d\d\d\d)',r'(\d\d\d\d\d)-(\d\d\d\d)',r'(\d\d\d\d)-(\d\d\d\d)']

print(white+'['+blue+'*'+white+'] - Starting searching...\n')

with open('phoneNumber.txt','r') as file_phone:
	for line in file_phone:
		for regex in regexs:
			phoneNumberRegex = re.compile(regex)
			mo = phoneNumberRegex.search(line)
			try:
				print(white+'['+green+'+'+white+'] - Number Found! ==> {}'.format(mo.group()))
			except:
				pass
