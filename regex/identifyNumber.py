def identify_number(number):
	if len(number) != 10:
		return False
	for i in range(0,5):
		if not number[i].isdecimal():
			return False
	if number[5] != "-":
		return False
	for i in range(6,10):
		if not number[i].isdecimal():
			return False
	return True
message = "Ei washington ligue-me por 99967-4626 amanhã. 99944-4673 meu escritório"
for i in range(len(message)):
	chunk = message[i:i+10]
	if identify_number(chunk):
		print("Número encontrado: "+chunk)
print("Pronto")
